import { FIELDS, FIELDS_SCALED } from './fields.js';

const PIECE_WIDTH = 74;
const PIECE_HEIGHT = 105;
const PIECES_OFFSETS = [
  {x: 0, y: -40},
  {x: -20, y: -20},
  {x: 20, y: -20},
  {x: 0, y: 0},
];

export default class Pieces {
  constructor(container) {
    this.container = container;
    this.pieces = [];
    this.scale = 1.0;
  }

  add(color, name, pc=false) {
    const piece_element = document.createElement('div');
    piece_element.classList.add('piece', color, 'notransition');
    this.pieces.push({
      element: piece_element,
      color: color,
      position: 0,
      waits: 0,
      name: name,
      pc: pc,
    });
    this.container.appendChild(piece_element);
    this.move(this.pieces.length-1, 0);
  }

  move(piece_id, field_id) {
    if (field_id > FIELDS.length-1) field_id = FIELDS.length - 1;
    const field_pos = FIELDS_SCALED[field_id];
    const x = field_pos.x - PIECE_WIDTH / 2 * this.scale + PIECES_OFFSETS[piece_id].x * this.scale;
    const y = field_pos.y - PIECE_HEIGHT / 2 * this.scale + PIECES_OFFSETS[piece_id].y * this.scale;
    const element = this.pieces[piece_id].element;
    element.style.left = `${x}px`;
    element.style.top = `${y}px`;
    this.pieces[piece_id].position = field_id;
    element.offsetHeight;
    element.classList.remove('notransition');
  }

  refreshPositions() {
    this.pieces.forEach( (piece, index) => {
      piece.element.classList.add('notransition');
      this.move(index, piece.position);
    })
  }
}