import { addFields, FIELDS, FIELDS_SCALED } from './fields.js';
import Pieces from './pieces.js';

const MAX_INFOS = 4;
const board_container = document.querySelector('.board-container');
const board = document.querySelector('.board');
const info = document.querySelector('.info');
const players_list = document.querySelector('.players-list');
const audio = new Audio('DICE.WAV');


let playersElements = [];
let player = 0;
let prev_player = 0;
let can_click = true;
let last_info = null;

show_info('Rozpoczynamy grę!');
function show_info(text) {
  const element = document.createElement('div');
  element.innerHTML = text;
  if (last_info !== null) {
    if(info.childNodes.length === MAX_INFOS) {
      info.lastElementChild.remove();
    };

    last_info.parentNode.insertBefore(element, last_info);
    console.log('now');
  } else {
    info.appendChild(element);
  }  
  last_info = element;
}

function make_move() {
  if (gameOver()) {
    show_info('Gra zakończona.');
    pieces.pieces[player].element.classList.remove('active');
    return;
  }

  if (pieces.pieces[player].position === FIELDS.length - 1) {
    nextPlayer();
    return;
  }
  
  if (pieces.pieces[player].waits > 0) {
    const piece = pieces.pieces[player];
    show_info(`<span style="color:${piece.color}">${piece.name}</span> czeka kolejkę.`);
    pieces.pieces[player].waits -= 1;
    nextPlayer();
    return;
  }
  can_click = false;


  const roll = Math.floor(Math.random() * 6) + 1;
  changeSide(roll);
  
  setTimeout( function() {
    const piece = pieces.pieces[player];
    show_info(`<span style="color:${piece.color}">${piece.name}</span> wyrzucił ${roll}.`);
    let new_pos = pieces.pieces[player].position + roll;
    if (new_pos > FIELDS.length -1) {
      new_pos = FIELDS.length -1;
    }
    let field = FIELDS[new_pos];
    
    pieces.move(player, new_pos);
    setTimeout(function(){
      const piece = pieces.pieces[player];
      if (field.type === 'normal') nextPlayer();
      if (field.type === 'move_by') {
        if (field.move > 0) {
          show_info(`<span style="color:${piece.color}">${piece.name}</span> przesuwa się do przedu o ${field.move}.`);
        } else {
          show_info(`<span style="color:${piece.color}">${piece.name}</span> cofa się o ${-field.move}.`);
        }
        moveTo(new_pos + field.move);
      }
      if (field.type === 'move_to') {
        show_info(`<span style="color:${piece.color}">${piece.name}</span> przesuwa się na pole ${field.move + 1}.`);
        moveTo(field.move);
      }
      if (field.type === 'waits') {
        pieces.pieces[player].waits = field.waits;
        if (field.waits === 1) {
          show_info(`<span style="color:${piece.color}">${piece.name}</span> traci ${field.waits} kolejkę.`);
        } else {
          show_info(`<span style="color:${piece.color}">${piece.name}</span> traci ${field.waits} kolejki.`);
        }
        
        nextPlayer();
      }
    }, 500);
  }, 1000);
}

board_container.addEventListener('click', (e) => {
  if (!can_click) return;
  make_move();
})

function gameOver() {
  return !pieces.pieces.some( function (piece) {
    return piece.position < FIELDS.length -1;
  })
}

function moveTo(field_id) {
  pieces.move(player, field_id);
  setTimeout(function () {
    nextPlayer();
  }, 500);
}

function nextPlayer() {
  pieces.pieces[player].element.classList.remove('active');
  player += 1;
  if (player > pieces.pieces.length -1) player = 0;
  setActivePlayer(player);
  if (pieces.pieces[player].pc) {
    make_move();
  } else {
    can_click = true;
  }
}

addFields(board_container, false);
const pieces = new Pieces(board_container);

resizeElements();


const COLORS = [
  'red',
  'blue',
  'yellow',
  'green'
]
let colors_left = [...COLORS];

function getRandomColor() {
  if (colors_left.length === 0) return;
  const color_index = Math.floor(Math.random() * colors_left.length);
  const color = colors_left[color_index];
  colors_left.splice(color_index, 1);
  return color;
}

//add_player('red', 'Łukasz');
//add_player('blue', 'Kuba');
//add_player('yellow', 'Tata');
// add_player('yellow', true);

add_player(getRandomColor(), 'Gracz 1');
//add_player(getRandomColor(), 'Gracz 2');
add_player(getRandomColor(), 'Komputer 1', true);
//add_player(getRandomColor(), 'Komputer 2', true);
//add_player(getRandomColor(), 'Komputer 3', true);
playersElements = document.querySelectorAll('.players-list li');
//pieces.move(0, 40);
//pieces.move(1, 40);

function add_player(color, name, pc=false) {
  pieces.add(color, name, pc);

  const player = document.createElement('li');
  // <li><span class="player-icon blue"></span><span class="player-name">Kuba</span></li>

  const icon_element = document.createElement('span');
  icon_element.classList.add('player-icon', color);
  player.appendChild(icon_element);

  const name_element = document.createElement('span');
  name_element.classList.add('player-name');
  name_element.innerText = name;
  player.appendChild(name_element);

  players_list.appendChild(player);

}

setActivePlayer(player);
function setActivePlayer(player) {
  playersElements[prev_player].classList.remove('active');
  prev_player = player;
  playersElements[player].classList.add('active');
  pieces.pieces[player].element.classList.add('active');
};

//  pieces.pieces.forEach(piece => {
//    piece.position = 50;
//  });

window.addEventListener('resize', resizeElements);

function resizeElements() {
  const map_width = 1973;
  const map_height = 1549;
  const map_ratio = map_width / map_height;
  const aktual_width = board.offsetWidth;
  const aktual_height = board.offsetHeight;
  const aktual_ratio = aktual_width / aktual_height;
  const size_by = (map_ratio > aktual_ratio) ? 'W' : 'H';
  let margin_top = 0;
  let margin_left = 0;
  let scale = 1.0;
  if (size_by === 'W') {
    scale = aktual_width / map_width;
    margin_top = Math.round((aktual_height - scale * map_height) / 2);
  } else {
    scale = aktual_height / map_height;
    margin_left = Math.round((aktual_width - scale * map_width) / 2);
  }
  
  document.documentElement.style.setProperty('--scale', scale);
  document.documentElement.style.setProperty('--margin-top', `${margin_top}px`);
  document.documentElement.style.setProperty('--margin-left', `${margin_left}px`);  
  document.documentElement.style.setProperty('--circle-size', Math.round(90 * scale) + 'px');
  document.documentElement.style.setProperty('--piece-width', Math.round(74 * scale) + 'px');
  document.documentElement.style.setProperty('--piece-height', Math.round(105 * scale) + 'px')

  FIELDS_SCALED.forEach( (field, index) => {
    field.x = FIELDS[index].x * scale + margin_left;
    field.y = FIELDS[index].y * scale + margin_top;
    //field.x = FIELDS[index].x * scale;
    const circle = document.getElementById(`circle-${index}`);
    //circle.style.top = `${field.y * scale + margin_top}px`;
    //circle.style.left = `${field.x * scale + margin_left}px`;
    if (circle) {
      circle.style.top = `${field.y}px`;
      circle.style.left = `${field.x}px`;
    }
  })

  if (pieces) {
    pieces.scale = scale;
    pieces.refreshPositions();
  }

}

const dice = document.querySelector('.dice');
let currentClass = 'dice-3';

function changeSide(side) {
  audio.play();
  const randomSides = [1, 2, 3, 4, side];
  randomSides.forEach((side, index) => {
    setTimeout(function () {
      dice.classList.remove(currentClass);
      currentClass = `dice-${side}`;
      dice.classList.add(currentClass);
      
    }, 100*index);
  })
}
