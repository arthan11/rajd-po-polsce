
export const FIELDS = [
  {x: 1580, y: 1415, type: 'normal'},
  {x: 1467, y: 1420, type: 'normal'},
  {x: 1359, y: 1421, type: 'normal'},
  {x: 1256, y: 1403, type: 'move_by', move: -3},
  {x: 1153, y: 1379, type: 'normal'},
  {x: 1059, y: 1329, type: 'move_by', move: 4}, // Zakopane
  {x: 965, y: 1285, type: 'normal'},
  {x: 886, y: 1223, type: 'normal'},
  {x: 929, y: 1125, type: 'move_by', move: 4}, // Kraków
  {x: 984, y: 1032, type: 'normal'},

  {x: 908, y: 954, type: 'move_by', move: 4}, // Częstochowa
  {x: 802, y: 956, type: 'normal'},
  {x: 712, y: 1014, type: 'normal'},
  {x: 605, y: 1051, type: 'normal'},
  {x: 498, y: 1048, type: 'move_by', move: -3},
  {x: 401, y: 1006, type: 'move_by', move: 4}, // Wrocław
  {x: 365, y: 901, type: 'move_to', move: 38}, // 17
  {x: 392, y: 795, type: 'normal'},
  {x: 487, y: 763, type: 'normal'},
  {x: 587, y: 798, type: 'normal'},

  {x: 690, y: 815, type: 'normal'},
  {x: 795, y: 833, type: 'normal'},
  {x: 900, y: 820, type: 'normal'},
  {x: 1000, y: 850, type: 'move_by', move: -3},
  {x: 1090, y: 897, type: 'move_by', move: 4}, // Łódź
  {x: 1177, y: 946, type: 'normal'},
  {x: 1287, y: 989, type: 'normal'},
  {x: 1390, y: 1031, type: 'move_to', move: 43}, // 28
  {x: 1493, y: 1060, type: 'normal'},
  {x: 1608, y: 1078, type: 'normal'},

  {x: 1719, y: 1078, type: 'normal'},
  {x: 1780, y: 983, type: 'waits', waits: 1}, // 32
  {x: 1711, y: 892, type: 'normal'},
  {x: 1622, y: 830, type: 'move_by', move: 4}, // Lublin
  {x: 1520, y: 796, type: 'normal'},
  {x: 1450, y: 717, type: 'normal'},
  {x: 1535, y: 638, type: 'normal'},
  {x: 1629, y: 570, type: 'normal'},
  {x: 1573, y: 469, type: 'normal'}, // 39
  {x: 1465, y: 461, type: 'move_by', move: -3},

  {x: 1415, y: 553, type: 'normal'},
  {x: 1346, y: 635, type: 'normal'},
  {x: 1242, y: 613, type: 'move_by', move: 4}, // Warszawa
  {x: 1189, y: 519, type: 'normal'}, // 44
  {x: 1253, y: 435, type: 'normal'},
  {x: 1336, y: 372, type: 'waits', waits: 2}, // 46
  {x: 1313, y: 270, type: 'normal'},
  {x: 1217, y: 215, type: 'normal'},
  {x: 1115, y: 215, type: 'move_by', move: 4}, // Olsztyn
  {x: 1063, y: 310, type: 'normal'},

  {x: 1029, y: 414, type: 'normal'},
  {x: 970, y: 516, type: 'move_by', move: 4}, // Toruń
  {x: 862, y: 524, type: 'normal'},
  {x: 759, y: 494, type: 'move_by', move: 4}, // Bydgoszcz
  {x: 672, y: 559, type: 'normal'},
  {x: 565, y: 560, type: 'normal'},
  {x: 456, y: 557, type: 'move_by', move: 4}, // Poznań
  {x: 345, y: 547, type: 'normal'},
  {x: 250, y: 495, type: 'normal'},
  {x: 186, y: 408, type: 'move_by', move: 4}, // Szczecin

  {x: 238, y: 309, type: 'normal'},
  {x: 346, y: 293, type: 'normal'},
  {x: 460, y: 300, type: 'normal'},
  {x: 569, y: 326, type: 'normal'},
  {x: 679, y: 327, type: 'move_by', move: -3},
  {x: 786, y: 327, type: 'normal'},
  {x: 890, y: 312, type: 'normal'},
  {x: 855, y: 211, type: 'normal'}, // Gdańsk / Koniec
];

export const FIELDS_SCALED = [];

export function addFields(container, visible=true) {

  FIELDS.forEach( (field, index) => {
    if (visible) {
      let field_element = document.createElement('div');
      field_element.classList.add('circle')
      field_element.style.left = `${field.x}px`;
      field_element.style.top = `${field.y}px`;
      field_element.setAttribute('id', `circle-${index}`);
      container.appendChild(field_element);  
    }
    FIELDS_SCALED.push({
      x: field.x,
      y: field.y
    })
  });
  

}